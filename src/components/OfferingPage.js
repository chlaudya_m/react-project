import React from "react";
import "../assets/css/all.css";
import "../assets/css/style-mp1.css";

const OfferingPage = () => {
    return (
        <section class="offering-wrapper">
            <div class="container offering flex">
                <div class="offering-title-wrapper">
                    <h3 class="offering-title">
                        Awali kesuksesan online Anda bersama Niagahoster!
                    </h3>
                </div>

                <div class="btn-wrapper">
                    <a href="" class="offering-btn orange">mulai sekarang</a>
                </div>
            </div>
        </section>
    )
}

export default OfferingPage;