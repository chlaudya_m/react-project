import React from "react";
import "../assets/css/all.css";
import "../assets/css/style-mp1.css";

const PriorityPage = () => {
    return (
        <section class="priority-wrapper">
            <div class="container">
                <h3 class="heading">
                    Prioritas Kecepatan dan Keamanan
                </h3>
                <div class="priority-content-wrapper flex">
                    <div class="priority-feature-wrapper">
                        <div class="priority-feature">
                            <h4 class="priority-feature-title">
                                Hosting Super Cepat
                            </h4>
                            <p class="priority-description">
                                Pengunjung tidak suka website lambat. Dengan dukungan
                                LiteSpeed Web Server, waktu loading website Anda akan
                                meningkat pesat.
                            </p>
                        </div>

                        <div class="priority-feature">
                            <h4 class="priority-feature-title">
                                Keamanan Website Extra
                            </h4>
                            <p class="priority-description">
                                Teknologi keamanan Imunify 360 memungkinkan Website anda
                                terlindung dari serangan hacker, malware, dan virus berbahaya
                                setiap saat.
                            </p>
                        </div>
                        <a href="" class="btn-hero orange">Lihat Selengkapnya</a>
                    </div>
                    <div class="priority-image-wrapper">
                        <img src={process.env.PUBLIC_URL + '/server.webp'} alt="" class="priority-server-img" />
                        <img src={process.env.PUBLIC_URL + '/graphic.svg'} alt="" class="priority-graphic-img" />
                        <img src={process.env.PUBLIC_URL + '/imunify.svg'} alt="" class="priority-imunify-img" />
                        <img src={process.env.PUBLIC_URL + '/lite-speed.svg'} alt="" class="priority-speed-img" />
                    </div>
                </div>
            </div>
        </section>
    )

}
export default PriorityPage;