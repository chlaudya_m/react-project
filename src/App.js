import React, { useState } from 'react';
import "./App.css";
import MyRoutes from "./route/MyRoutes"


const App = () => {
    return (
        <div className="App">
            <MyRoutes />
        </div>
    )
}
export default App;