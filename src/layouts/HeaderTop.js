import React from "react";
import { Link } from "react-router-dom";
import "../assets/css/all.css";
import "../assets/css/style-mp1.css";

const HeaderTop = () => {
    return (
        <div className="nav-wrapper">
            <div class="container">
                <nav class="sub-nav">
                    <span><i class="fas fa-phone"></i>0274-2885822</span>
                    <span><i class="fas fa-comment-alt"></i>Live Chat</span>
                    <i class="fas fa-shopping-cart"></i>
                </nav>
                <nav class="main-nav flex">
                    <a href="/" class="nav-brand-wrapper">
                        <img src={process.env.PUBLIC_URL + '/nh-logo.svg'} alt="" class="hero-img" />
                    </a>
                    <div class="navigation flex">
                        <Link to="/"></Link>
                        <Link to="/unlimited">Unlimited Hosting</Link>
                        <Link to="/hosting">CLOUD HOSTING</Link>
                        <Link to="/vps">CLOUD VPS</Link>
                        <Link to="/domain">DOMAIN</Link>
                        <Link to="/afiliasi">AFILIASI</Link>
                        <Link to="/blog">BLOG</Link>
                        <button className="login"> LOGIN </button>
                    </div>
                </nav>
            </div>
        </div>
    )

}
export default HeaderTop;