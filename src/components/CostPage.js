import React from "react";
import "../assets/css/all.css";
import "../assets/css/style-mp1.css";

const CostPage = () => {
    return (
        <section class="cost-saving-wrapper">
            <div class="container">
                <h3 class="heading">
                    Biaya Hemat Kualitas Hebat
                </h3>
                <div class="cost-saving-content-wrapper flex">
                    <div class="cost-saving-image-wrapper">
                        <img src={process.env.PUBLIC_URL + '/titis.webp'} alt="" class="cost-titis-img" />
                        <img src={process.env.PUBLIC_URL + '/woman1.webp'} alt="" class="cost-woman1-img" />
                        <img src={process.env.PUBLIC_URL + '/woman2.png'} alt="" class="cost-woman2-img" />
                        <img src={process.env.PUBLIC_URL + '/man.png'} alt="" class="cost-man-img" />
                        <img src={process.env.PUBLIC_URL + '/icon-online.svg'} alt="" class="cost-online-img" />
                        <img src={process.env.PUBLIC_URL + '/intercom-logo.svg'} alt="" class="cost-intercom-img" />
                    </div>

                    <div class="cost-saving-feature-wrapper">
                        <div class="cost-saving-feature">
                            <h4 class="cost-saving-feature-title">
                                Harga Murah Fitur Lengkap
                            </h4>
                            <p class="cost-saving-description">
                                Anda bisa berhemat dan tetap mendapatkan hosting terbaik
                                dengan fitur lengkap, dari auto install WordPress, cPanel
                                lengkap, hingga SSL gratis
                            </p>
                        </div>
                        <div class="cost-saving-feature">
                            <h4 class="cost-saving-feature-title">
                                Website Selalu Online
                            </h4>
                            <p class="cost-saving-description">
                                Jaminan server uptime 99,98% memungkinkan Website Anda selalu
                                online sehingga Anda tidak perlu khawatir kehilangan traffic
                                dan pendapatan.
                            </p>
                        </div>
                        <div class="cost-saving-feature">
                            <h4 class="cost-saving-feature-title">
                                Tim Support Handal dan Cepat Tanggap
                            </h4>
                            <p class="cost-saving-description">
                                Tidak perlu menunggu lama, selesaikan masalah Anda dengan
                                cepat secara real-time melalui live chat 24/7
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )

}
export default CostPage;