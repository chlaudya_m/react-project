import React from "react";
import "../assets/css/all.css";
import "../assets/css/style-mp1.css";

const PaymentPage = () => {
    return (
        <section class="pembayaran-wrapper">
            <div class="container">
                <h2 class="pembayaran-title">
                    Beragam pilihan cara pembayaran yang mempercepat proses order
                    hosting & domain anda.
          </h2>
                <div class="pembayaran-img-wrapper flex">
                    <img src={process.env.PUBLIC_URL + '/bca.svg'} alt="" class="pembayaran-img" />
                    <img src={process.env.PUBLIC_URL + '/mandiri.svg'} alt="" class="pembayaran-img" />
                    <img src={process.env.PUBLIC_URL + '/bni.svg'} alt="" class="pembayaran-img" />
                    <img src={process.env.PUBLIC_URL + '/bri.svg'} alt="" class="pembayaran-img" />
                    <img src={process.env.PUBLIC_URL + '/bii.svg'} alt="" class="pembayaran-img" />
                    <img src={process.env.PUBLIC_URL + '/cimb.svg'} alt="" class="pembayaran-img" />
                    <img src={process.env.PUBLIC_URL + '/alto.svg'} alt="" class="pembayaran-img" />
                    <img src={process.env.PUBLIC_URL + '/atm-bersama.svg'} alt="" class="pembayaran-img" />
                    <img src={process.env.PUBLIC_URL + '/paypal.svg'} alt="" class="pembayaran-img" />
                    <img src={process.env.PUBLIC_URL + '/indomart.svg'} alt="" class="pembayaran-img" />
                    <img src={process.env.PUBLIC_URL + '/alfamart.svg'} alt="" class="pembayaran-img" />
                    <img src={process.env.PUBLIC_URL + '/pegadaian.svg'} alt="" class="pembayaran-img" />
                    <img src={process.env.PUBLIC_URL + '/pos.svg'} alt="" class="pembayaran-img" />
                    <img src={process.env.PUBLIC_URL + '/ovo.svg'} alt="" class="pembayaran-img" />
                    <img src={process.env.PUBLIC_URL + '/gopay.svg'} alt="" class="pembayaran-img" />
                    <img src={process.env.PUBLIC_URL + '/visa.svg'} alt="" class="pembayaran-img" />
                    <img src={process.env.PUBLIC_URL + '/master.svg'} alt="" class="pembayaran-img" />
                </div>
            </div>
        </section>
    )
}

export default PaymentPage;