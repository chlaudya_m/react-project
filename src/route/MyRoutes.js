import React from "react";
import { Route } from "react-router-dom";
import HeaderTop from "../layouts/HeaderTop";
import FooterBottom from "../layouts/FooterBottom";
import UnlimitedHosting from "../views/UnlimitedHosting";
import HomePage from "../views/HomePage";
import CloudHosting from "../views/CloudHosting";
import CloudVps from "../views/CloudVps";
import DomainPage from "../views/DomainPage";
import AfiliasiPage from "../views/AfiliasiPage";
import BlogPage from "../views/BlogPage";

const MyRoutes = () => {
    return (
        <div>
            <HeaderTop />
            <Route path="/" component={HomePage} exact />
            <Route path="/unlimited" component={UnlimitedHosting} exact />
            <Route path="/hosting" component={CloudHosting} exact />
            <Route path="/vps" component={CloudVps} exact />
            <Route path="/domain" component={DomainPage} exact />
            <Route path="/afiliasi" component={AfiliasiPage} exact />
            <Route path="/blog" component={BlogPage} exact />
            <FooterBottom />
        </div>
    )
}
export default MyRoutes;