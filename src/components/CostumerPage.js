import React from "react";
import "../assets/css/all.css";
import "../assets/css/style-mp1.css";

const CostumerPage = () => {
    return (
        <section class="customer-wrapper">
            <div class="container">
                <h3 class="heading">
                    Kata Pelanggan Tentang Niagahoster
                </h3>
                <div class="customer-content-wrapper flex">
                    <div class="customer">
                        <img src={process.env.PUBLIC_URL + '/devjavu.webp'} alt="" class="devjavu-img" />
                        <p class="customer-testimony">
                            Website itu sangat penting bagi UMKM sebagai promosi untuk memenangkan persaingan di era global digital
                        </p>
                        <p class="owner">Didik & Johan <span>Owner Devjavu</span></p>
                    </div>
                    <div class="customer">
                        <img src={process.env.PUBLIC_URL + '/optimizer.webp'} alt="" class="optimizer-img" />
                        <p class="customer-testimony">
                            Bagi saya Niagahoster bukan sekadar penyedia hosting, melainkan
                            partner bisnis yang bisa dipercaya.
                        </p>
                        <p class="owner">
                            Bob Setyo <span>Owner Digital Optimizer Indonesia</span>
                        </p>
                    </div>
                    <div class="customer">
                        <img src={process.env.PUBLIC_URL + '/sateratu.webp'} alt="" class="sate-ratu-img" />
                        <p class="customer-testimony">
                            Solusi yang diberikan tim support Niagahoster sangat mudah
                            dimengerti buat saya yang tidak paham teknis.
                        </p>
                        <p class="owner">Budi Saputro <span>Owner Sate Ratu</span></p>
                    </div>
                </div>
            </div>
        </section>
    )
}
export default CostumerPage;