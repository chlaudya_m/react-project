import React from "react";
import "../assets/css/all.css";
import "../assets/css/style-mp1.css";

const GaransiPage = () => {
    return (
        <section class="garansi-wrapper">
            <div class="container">
                <div class="garansi-konten-wrapper flex">
                    <div class="garansi-img-wrapper">
                        <img
                            src={process.env.PUBLIC_URL + '/icons-guarantee.svg'}
                            alt=""
                            class="garansi-img"
                        />
                    </div>
                    <div class="garansi-deskripsi-wrapper">
                        <h3 class="heading">Garansi 30 Hari Uang kembali</h3>
                        <p class="garansi-deskripsi">
                            Tidak puas dengan layanan Niagahoster? Kami menyediakan garansi
                            uang kembali yang berlaku 30 hari sejak tanggal pembelian
              </p>
                        <a href="" class="btn orange">Mulai Sekarang</a>
                    </div>
                </div>
            </div>
        </section>
    )
}
export default GaransiPage;