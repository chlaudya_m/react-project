import React from "react";
import "../assets/css/all.css";
import "../assets/css/style-mp1.css";

const HeroPage = () => {
    return (
        <header class="document-header-wrapper">
            <section class="contact-header">
                <section class="hero container flex">
                    <section class="hero-left">
                        <h1 class="hero-title">
                            Unlimited Web Hosting Terbaik di Indonesia
                        </h1>
                        <p class="hero-description">
                            Ada banyak peluang bisa Anda raih dari rumah dengan memiliki
                            website. Manfaatkan diskon hosting hingga 75% dan tetap produktif
                            di bulan Ramadhan bersama niagahoster.
                        </p>
                        <br />
                        <p>Yuk segera order karena diskon dapat berakhir sewaktu-waktu!</p>
                        <div class="countdown flex">
                            <p>00</p>
                            <p>:</p>
                            <p>00</p>
                            <p>:</p>
                            <p>00</p>
                            <p>:</p>
                            <p>00</p>
                        </div>
                        <a href="" class="btn-hero btn-1 orange">Pilih Sekarang</a>
                    </section>
                    <section class="hero-right">
                        <div class="hero-img-wrapper">
                            <img src={process.env.PUBLIC_URL + '/hero-home-ramadhan.webp'} alt="" class="hero-img" />
                        </div>
                    </section>
                </section>
            </section>
        </header>
    )
}
export default HeroPage;