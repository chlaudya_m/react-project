import React from "react";
import HeroPage from "../components/HeroPage";
import LayananPage from "../components/LayananPage";
import PriorityPage from "../components/PriorityPage";
import CostPage from "../components/CostPage";
import CostumerPage from "../components/CostumerPage";
import PackagePage from "../components/PackagePage";
import ClientPage from "../components/ClientPage";
import FaqPage from "../components/FaqPage";
import GaransiPage from "../components/GaransiPage";
import PaymentPage from "../components/PaymentPage";
import OfferingPage from "../components/OfferingPage";


const HomePage = () => {
    return (
        <div className="home">
            <HeroPage />
            <LayananPage />
            <PriorityPage />
            <CostPage />
            <CostumerPage />
            <PackagePage />
            <ClientPage />
            <FaqPage />
            <GaransiPage />
            <PaymentPage />
            <OfferingPage />
        </div>
    )

}
export default HomePage;