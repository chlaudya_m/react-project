import React from "react";
import "../assets/css/all.css";
import "../assets/css/style-mp1.css";

const LayananPage = () => {

    return (
        <section class="layanan-wrapper">
            <div class="container">
                <h3 class="heading">
                    Layanan Niagahoster
          </h3>
                <div class="card-wrapper">
                    <div class="card-column-wrapper flex">
                        <div class="card layanan">
                            <figure class="layanan-icon-wrapper">
                                <img src={process.env.PUBLIC_URL + '/icon-1.svg'} alt="" class="layanan-icon" />
                            </figure>
                            <figcaption>Unlimited Hosting</figcaption>
                            <p class="layanan-deskripsi">
                                Cocok untuk website skala kecil dan menengah
                            </p>
                            <p class="layanan-price-intro">Mulai dari</p>
                            <p class="layanan-price">Rp 10.000,-</p>
                        </div>
                        <div class="card layanan">
                            <figure class="layanan-icon-wrapper">
                                <img src={process.env.PUBLIC_URL + '/icons-cloud-hosting.svg'} alt="" class="layanan-icon" />
                            </figure>
                            <figcaption>Cloud Hosting</figcaption>
                            <p class="layanan-deskripsi">
                                Kapasitas resource tinggi, fully managed, dan mudah dikelola
                            </p>
                            <p class="layanan-price-intro">Mulai dari</p>
                            <p class="layanan-price">Rp 150.000,-</p>
                        </div>
                        <div class="card layanan">
                            <figure class="layanan-icon-wrapper">
                                <img src={process.env.PUBLIC_URL + '/icons-cloud-vps.svg'} alt="" class="layanan-icon" />
                            </figure>
                            <figcaption>Cloud VPS</figcaption>
                            <p class="layanan-deskripsi">
                                Dedicated resource dengan akses root dan konfigurasi mandiri
                            </p>
                            <p class="layanan-price-intro">Mulai dari</p>
                            <p class="layanan-price">Rp 104.000,-</p>
                        </div>
                        <div class="card layanan">
                            <figure class="layanan-icon-wrapper">
                                <img src={process.env.PUBLIC_URL + '/icons-domain.svg'} alt="" class="layanan-icon" />
                            </figure>
                            <figcaption>Domain</figcaption>
                            <p class="layanan-deskripsi">
                                Temukan nama domain yang anda inginkan
                            </p>
                            <p class="layanan-price-intro">Mulai dari</p>
                            <p class="layanan-price">Rp 14.000,-</p>
                        </div>
                    </div>
                    <div class="card layanan-row">
                        <figure class="layanan-icon-wrapper">
                            <img src={process.env.PUBLIC_URL + '/icons-cloud-hosting.svg'} alt="" class="layanan-icon" />
                        </figure>
                        <div class="description-wrapper-row-card">
                            <figcaption>Pembuatan Website</figcaption>
                            <p class="layanan-deskripsi">
                                500 perusahaan lebih percayakan pembuatan websitenya pada kami
                            <a href="">Cek Selengkapnya</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    )
}

export default LayananPage;