import React from "react";
import "../assets/css/all.css";
import "../assets/css/style-mp1.css";

const FaqPage = () => {
    return (
        <section class="faq-wrapper">
            <h3 class="heading">Pertanyaan yang sering diajukan</h3>
            <div class="container">
                <div class="question-wrapper">
                    <ul>
                        <li class="question">Apa itu web hosting?</li>
                        <img src={process.env.PUBLIC_URL + '/icon-plus.svg'} alt="" class="img-right" />
                        <hr />
                        <li class="question">
                            Mengapa saya harus menggunakan web hosting Indonesia?
                        </li>
                        <img src={process.env.PUBLIC_URL + '/icon-plus.svg'} alt="" class="img-right" />
                        <hr />
                        <li class="question">
                            Apa yang dimaksud dengan Unlimited Hosting di Niagahoster?
                        </li>
                        <img src={process.env.PUBLIC_URL + '/icon-plus.svg'} alt="" class="img-right" />
                        <hr />
                        <li class="question">
                            Paket web hosting apa yang cocok untuk saya?
                        </li>
                        <img src={process.env.PUBLIC_URL + '/icon-plus.svg'} alt="" class="img-right" />
                        <hr />
                        <li class="question">
                            Apakah semua pembelian mendapatkan domain gratis?
                        </li>
                        <img src={process.env.PUBLIC_URL + '/icon-plus.svg'} alt="" class="img-right" />
                        <hr />
                        <li class="question">
                            Jika saya sudah memiliki website, apakah saya bisa transfer web
                            hosting ke Niagahoster?
                        </li>
                        <img src={process.env.PUBLIC_URL + '/icon-plus.svg'} alt="" class="img-right" />
                        <hr />
                        <li class="question">Apa saja layanan web hosting Niaghoster?</li>
                        <hr />
                        <img src={process.env.PUBLIC_URL + '/icon-plus.svg'} alt="" class="img-right-weird" />
                    </ul>
                </div>
            </div>
        </section>
    )
}
export default FaqPage;