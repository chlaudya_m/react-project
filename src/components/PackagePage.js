import React from "react";
import "../assets/css/all.css";
import "../assets/css/style-mp1.css";

const PackagePage = () => {
    return (
        <section class="package-wrapper">
            <h3 class="heading">
                Pilih Paket Hosting Anda
            </h3>
            <div class="container">
                <div class="card-wrapper flex">
                    <div class="card-package">
                        <p class="package-title">Termurah!</p>
                        <h5 class="package-name">Bayi</h5>
                        <p class="package-discount visibility-none">x</p>
                        <p class="package-price">Rp 10.000,-</p>
                        <hr />
                        <a href="" class="btn-hero orange">Pilih Sekarang</a>
                        <package class="package-description flex">
                            Sesuai untuk Pemula atau belajar Website
                        </package>
                        <div class="package-feature-wrapper">
                            <p class="package-feature">500 MB Disk Space</p>
                            <p class="package-feature">Unlimited Bandwidth</p>
                            <p class="package-feature">Unlimited Database</p>
                            <p class="package-feature">1 Domain</p>
                            <p class="package-feature">Instant Backup</p>
                            <p class="package-feature">Unlimited SSL gratis selamanya</p>
                        </div>
                        <a href="" class="package-detail">Lihat detail fitur</a>
                    </div>
                    <div class="card-package">
                        <p class="package-title">Diskon up to 34%</p>
                        <h5 class="package-name">Pelajar</h5>
                        <p class="package-discount">Rp 60.000,-</p>
                        <p class="package-price">Rp 40.223,-</p>
                        <hr />
                        <a href="" class="btn-hero orange">pilih sekarang</a>
                        <package class="package-description flex">Sesuai untuk Budget Minimal, Landing Page, Blog Pribadi</package>
                        <div class="package-feature-wrapper">
                            <p class="package-feature">Unlimited Disk Space</p>
                            <p class="package-feature">Unlimited Bandwidth</p>
                            <p class="package-feature">Unlimited POP3 Email</p>
                            <p class="package-feature">Unlimited Database</p>
                            <p class="package-feature">10 Addon Domain</p>
                            <p class="package-feature">Instant Backup</p>
                            <p class="package-feature">Domain Gratis</p>
                            <p class="package-feature">Unlimited SSL gratis selamanya</p>
                        </div>
                        <a href="" class="package-detail">Lihat detail fitur</a>
                    </div>
                    <div class="card-package">
                        <p class="package-title">Diskon upto 75%</p>
                        <h5 class="package-name">Personal</h5>
                        <p class="package-discount">Rp 106.250,-</p>
                        <p class="package-price">Rp 26.563,-</p>
                        <hr />
                        <a href="" class="btn-hero orange">pilih sekarang</a>
                        <package class="package-description flex">
                            Sesuai untuk Website Bisnis, UKM, Organisasi, Komunitas, Toko
                            Online, dll
                        </package>
                        <div class="package-feature-wrapper">
                            <p class="package-feature">Unlimited Disk Space</p>
                            <p class="package-feature">Unlimited Bandwidth</p>
                            <p class="package-feature">Unlimited POP3 Email</p>
                            <p class="package-feature">Unlimited Database</p>
                            <p class="package-feature">Unlimited Addon Domain</p>
                            <p class="package-feature">Instant Backup</p>
                            <p class="package-feature">Domain Gratis</p>
                            <p class="package-feature">Unlimited SSL gratis selamanya</p>
                            <p class="package-feature">SpamAssasin Mail Protection</p>
                        </div>
                        <a href="" class="package-detail">Lihat detail fitur</a>
                    </div>
                    <div class="card-package">
                        <p class="package-title">
                            Diskon up to 42%
                        </p>
                        <h5 class="package-name">
                            Bisnis
                        </h5>
                        <p class="package-discount">Rp 147.800,-</p>
                        <p class="package-price">Rp 85.724,-</p>
                        <hr />
                        <a href="" class="btn-hero orange">pilih sekarang</a>
                        <package class="package-description flex">Sesuai untuk Website Bisnis, Portal berita, Toko Online,dll</package>
                        <div class="package-feature-wrapper">
                            <p class="package-feature">Unlimited Disk Space</p>
                            <p class="package-feature">Unlimited Bandwidth</p>
                            <p class="package-feature">Unlimited POP3 Email</p>
                            <p class="package-feature">Unlimited Database</p>
                            <p class="package-feature">Unlimited Addon Domain</p>
                            <p class="package-feature">Magic Auto Backup & Restore</p>
                            <p class="package-feature">Domain Gratis</p>
                            <p class="package-feature">Unlimited SSL Gratis Selamanya</p>
                            <p class="package-feature">Prioritas Layanan Support</p>
                            <p class="package-feature">SpamAssasin Mail Protection</p>
                        </div>
                        <a href="" class="package-detail">Lihat detail fitur</a>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default PackagePage;